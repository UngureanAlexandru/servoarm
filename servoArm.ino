  void move360Servo(int servoNumber, int analogControllerPin, int controllerThreshold);
  void move180Servo(int servoNumber, int angle);
  void init180Servos();
  void test();

  const int servo0 = 4;
  const int servo1 = 5;
  const int servo2 = 6;
  const int servo3 = 7;

  int servo0Angle = 0;
  int servo1Angle = 0;
  int servo2Angle = 0;
  int servo3Angle = 0;

  const int joystickButton = 8;

  const float pulseWidth = 20;
  float vector = 0.0001;
  int clawState = -1;
  bool clawRetracted = true;
  
  const float servoType180Upper = 2.6;
  const float servoType180Lower = 0.4;

  const float servoType360Upper = 1.55;
  const float servoType360Lower = 1.35;

  const int joystickThreshold = 500;
  const int joystickAcceptedError = 10;

  void setup() {
    Serial.begin(9600);
    pinMode(servo0, OUTPUT);
    pinMode(servo1, OUTPUT);
    pinMode(servo2, OUTPUT);
    pinMode(servo3, OUTPUT);

    pinMode(joystickButton, INPUT);
    digitalWrite(joystickButton, 1);

    init180Servos();
  }

void loop() {
    int horizontalInput = analogRead(A0);

    horizontalInput = horizontalInput > 980 ? 980 : horizontalInput;

    move360Servo(servo0, horizontalInput, joystickThreshold);

    int joystickHorizontal = analogRead(A1);

    if (joystickHorizontal > 600) {   
      if (servo1Angle < 160) {
          servo1Angle++;  
      }
    } else if(joystickHorizontal < 400) {
        if (servo1Angle > 5) {
          servo1Angle--; 
        }
    }

    move180Servo(servo1, servo1Angle);

    if (digitalRead(joystickButton) == 0 && clawState == -1) {
        clawState = 1;
        delay(200);
    } else if (digitalRead(joystickButton) == 0 && clawState == 1) {
        clawState = -1;
        delay(200);
    }
  
    moveClaw(clawState);
}

  void move360Servo(int servoNumber, int analogControllerValue, int controllerThreshold) {
    float dutyCycle = (servoType360Upper + servoType360Lower) / 2;

    if (analogControllerValue > controllerThreshold + joystickAcceptedError ||
        analogControllerValue < controllerThreshold - joystickAcceptedError) {
          dutyCycle += (servoType360Upper - (servoType360Upper + servoType360Lower) / 2) / (1023 - controllerThreshold) * (analogControllerValue - controllerThreshold);
    }

    digitalWrite(servoNumber, 1);
    delayMicroseconds(dutyCycle *  1000);
    digitalWrite(servoNumber, 0);
    delayMicroseconds((pulseWidth - dutyCycle) * 1000);

    delay(20);
  }

  void move180Servo(int servoNumber, int angle) {
    if (angle > 170) {
      angle = 170;
    }

    if (angle < 0) {
      angle = 0;
    }

    float unit = (servoType180Upper - servoType180Lower) / 180;
    float dutyCycle = servoType180Lower + angle * unit;
    
    digitalWrite(servoNumber, 1);
    delayMicroseconds(dutyCycle *  1000);
    digitalWrite(servoNumber, 0);
    delayMicroseconds((pulseWidth - dutyCycle) * 1000);
  }

  void moveClaw(int action) {
      if (action != 1 && action != -1) {
          return;
      }

      if (action == -1 && clawRetracted) {
          return;
      } else if (action == 1) {
          clawRetracted = false;
      }

      float average = (servoType360Upper + servoType360Lower) / 2;
      
      float dutyCycle = average;
      dutyCycle += action * (servoType360Upper - average);

      digitalWrite(servo3, 1);
      delayMicroseconds(dutyCycle *  1000);
      digitalWrite(servo3, 0);
      delayMicroseconds((pulseWidth - dutyCycle) * 1000);

      if (action == -1) {
          clawRetracted = true;
      }
  }


  void init180Servos() {
    servo1Angle = 90;
    float unit = (servoType180Upper - servoType180Lower) / 180;
    
    float dutyCycle = servoType180Lower + servo1Angle * unit;

    digitalWrite(servo1, 1);
    delayMicroseconds(dutyCycle *  1000);
    digitalWrite(servo1, 0);
    delayMicroseconds((pulseWidth - dutyCycle) * 1000);
  }

  void test() {
    int angleVector = 1;
    float dutyCycle = (servoType180Upper + servoType180Lower) / 2;
    float vector = 0.01;

    while (1) {
      if (dutyCycle > servoType180Upper - 0.1 || dutyCycle < servoType180Lower + 0.1) {
        angleVector *= -1;
      }

      dutyCycle += (angleVector * vector);

      digitalWrite(servo1, 1);
      delayMicroseconds(dutyCycle *  1000);
      digitalWrite(servo1, 0);
      delayMicroseconds((pulseWidth - dutyCycle) * 1000);
    }
  }
